// Local
#import "OffersViewModel.h"
#import "API.h"
#import "Offer.h"
#import "DistinctOffer.h"
#import "Segment.h"
#import "Flight.h"

// Libraries
#import <ReactiveObjC/ReactiveObjC.h>

@implementation OffersViewModel

- (NSInteger)numberOfRowsInSection {
    return _offers.count;
}

- (RACSignal*)findOffersWithDepartureAirportCode:(NSString*)departureAirportCode
                              arrivalAirportCode:(NSString*)arrivalAirportCode
                                   departureDate:(NSDate *)departureDate
                                      returnDate:(NSDate *)returnDate {
    RACReplaySubject* subject = [RACReplaySubject subject];
    RACSignal* authSignal = [[API shared] loginWithUsername:@"[partner]||SOAPTEST"
                                                   password:@"[partner]||SOAPTEST"
                                                       hash:@"[partner]||SOAPTEST"
                                                disableHash:YES];

    [authSignal subscribeNext:^(id  _Nullable x) {
        NSString* token = (NSString*)x;
        RACSignal* offersSignal = [[API shared] findOffersWithToken:token
                                                      departureDate:departureDate
                                                         returnDate:returnDate
                                               departureAirportCode:departureAirportCode
                                                 arrivalAirportCode:arrivalAirportCode];
        [offersSignal subscribeNext:^(id  _Nullable x) {
            NSArray<Offer*>* offers = (NSArray<Offer*>*)x;
            NSMutableArray<DistinctOffer*>* distinctOffers = [NSMutableArray new];
            for (Offer* offer in offers) {
                NSArray<DistinctOffer*>* dOffers = [offer distinctOffers];
                [distinctOffers addObjectsFromArray:dOffers];
            }
            _offers = distinctOffers;
            NSLog(@"%ld offers parsed", [offers count]);
            NSLog(@"%ld distinct offers total", [_offers count]);
            [subject sendNext:[NSNumber numberWithInteger:[_offers count]]];
        } error:^(NSError * _Nullable error) {
            [subject sendError:error];
        } completed:^{
            [subject sendCompleted];
        }];
    } error:^(NSError * _Nullable error) {
        NSLog(@"Authorization error");
    }];
    return subject;
}

- (DistinctOffer*)offerAt:(NSInteger)index {
    if (index >= [_offers count])
        return nil;
    return [_offers objectAtIndex:index];
}

- (NSString*)departureTimeToForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return offer.flightTo.segments.firstObject.departureTime;
}

- (NSString*)arrivalTimeToForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return offer.flightTo.segments.lastObject.arrivalTime;
}

- (NSString*)departureAirportCodeToForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return offer.flightTo.segments.firstObject.departureAirportCode;
}

- (NSString*)arrivalAirportCodeToForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return offer.flightTo.segments.lastObject.arrivalAirportCode;
}

- (NSString*)departureTimeBackForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return offer.flightBack.segments.firstObject.departureTime;
}

- (NSString*)arrivalTimeBackForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return offer.flightBack.segments.lastObject.arrivalTime;
}

- (NSString*)departureAirportCodeBackForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return offer.flightBack.segments.firstObject.departureAirportCode;
}

- (NSString*)arrivalAirportCodeBackForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return offer.flightBack.segments.lastObject.arrivalAirportCode;
}

- (NSString*)priceForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return [NSString stringWithFormat:@"%ld", (long)offer.totalPrice];
}

- (NSString*)durationToStringForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return [NSString stringWithFormat:@"%ldч %ldмин", offer.durationTo / (60 * 60), (offer.durationTo / 60) % 60];
}

- (NSString*)durationBackStringForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return [NSString stringWithFormat:@"%ldч %ldмин", offer.durationBack / (60 * 60), (offer.durationBack / 60) % 60];
}

- (NSURL*)urlForOfferAtIndex:(NSInteger)index {
    DistinctOffer* offer = [self offerAt:index];
    if (offer == nil) return nil;
    return [NSURL URLWithString:offer.link];
}

@end
