// iOS
#import <Foundation/Foundation.h>

@class Search;

@interface SearchViewModel : NSObject

@property (strong, nonatomic) Search* searchModel;

- (NSString*)departureAirportCode;
- (NSString*)arrivalAirportCode;
- (NSDate*)departureDate;
- (NSDate*)returnDate;
- (void)setDepartureAirportCode:(NSString*)departureAirportCode;
- (void)setArrivalAirportCode:(NSString*)arrivalAirportCode;
- (void)setDepartureDate:(NSDate*)departureDate;
- (void)setReturnDate:(NSDate*)returnDate;

- (NSDateComponents*)departureDateComponents;
- (NSDateComponents*)returnDateComponents;

@end
