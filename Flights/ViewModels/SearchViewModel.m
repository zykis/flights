// Local
#import "SearchViewModel.h"
#import "Search.h"

@implementation SearchViewModel

- (id)init {
    self = [super init];
    if (self) {
        self.searchModel = [Search new];
    }
    return self;
}

- (NSString*)departureAirportCode {
    return self.searchModel.departureAirportCode;
}

- (NSString*)arrivalAirportCode {
    return self.searchModel.arrivalAirportCode;
}

- (NSDate*)departureDate {
    return self.searchModel.departureDate;
}

- (NSDate*)returnDate {
    return self.searchModel.returnDate;
}

- (NSDateComponents*)departureDateComponents {
    if (self.searchModel.departureDate == nil) return nil;
    NSCalendar* gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    return [gregorian components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:self.searchModel.departureDate];
}

- (NSDateComponents*)returnDateComponents {
    if (self.searchModel.returnDate == nil) return nil;
    NSCalendar* gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    return [gregorian components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:self.searchModel.returnDate];
}

- (void)setDepartureAirportCode:(NSString*)departureAirportCode {
    self.searchModel.departureAirportCode = departureAirportCode;
}

- (void)setArrivalAirportCode:(NSString*)arrivalAirportCode {
    self.searchModel.arrivalAirportCode = arrivalAirportCode;
}

- (void)setDepartureDate:(NSDate*)departureDate {
    self.searchModel.departureDate = departureDate;
}

- (void)setReturnDate:(NSDate*)returnDate {
    self.searchModel.returnDate = returnDate;
}


@end
