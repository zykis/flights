// Local
#import "SearchViewModel.h"

@class DistinctOffer;
@class RACSignal;

@interface OffersViewModel: NSObject

@property (strong, nonatomic) NSArray<DistinctOffer*>* offers;

- (NSInteger)numberOfRowsInSection;
- (RACSignal*)findOffersWithDepartureAirportCode:(NSString*)departureAirportCode
                              arrivalAirportCode:(NSString*)arrivalAirportCode
                                   departureDate:(NSDate*)departureDate
                                      returnDate:(NSDate*)returnDate;

- (NSString*)departureTimeToForOfferAtIndex:(NSInteger)index;
- (NSString*)arrivalTimeToForOfferAtIndex:(NSInteger)index;
- (NSString*)departureAirportCodeToForOfferAtIndex:(NSInteger)index;
- (NSString*)arrivalAirportCodeToForOfferAtIndex:(NSInteger)index;

- (NSString*)departureTimeBackForOfferAtIndex:(NSInteger)index;
- (NSString*)arrivalTimeBackForOfferAtIndex:(NSInteger)index;
- (NSString*)departureAirportCodeBackForOfferAtIndex:(NSInteger)index;
- (NSString*)arrivalAirportCodeBackForOfferAtIndex:(NSInteger)index;

- (NSString*)priceForOfferAtIndex:(NSInteger)index;

- (NSString*)durationToStringForOfferAtIndex:(NSInteger)index;
- (NSString*)durationBackStringForOfferAtIndex:(NSInteger)index;

- (NSURL*)urlForOfferAtIndex:(NSInteger)index;

@end
