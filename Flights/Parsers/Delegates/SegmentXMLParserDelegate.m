// Local
#import "SegmentXMLParserDelegate.h"
#import "Segment.h"
#import "FlightXMLParserDelegate.h"
#import "Flight.h"
#import "Keys.h"

@interface SegmentXMLParserDelegate()
@property (strong, nonatomic) Segment* segment;
@property (strong, nonatomic) NSString* currentProperty;
@property (strong, nonatomic) NSString* currentStringValue;
@property (strong, nonatomic) NSDateFormatter* formatter;
@end

@implementation SegmentXMLParserDelegate

- (id)init {
    self = [super init];
    if (self) {
        self.segment = [Segment new];
        self.formatter = [NSDateFormatter new];
        self.formatter.dateFormat = @"dd.MM.yyyy hh:mm:ss";
        self.formatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    }
    return self;
}

- (void)parser:(NSXMLParser*)parser
didStartElement:(nonnull NSString *)elementName
  namespaceURI:(nullable NSString *)namespaceURI
 qualifiedName:(nullable NSString *)qName
    attributes:(nonnull NSDictionary<NSString *,NSString *> *)attributeDict {
    if ([elementName isEqualToString:kFlightNumber]) {
        _currentProperty = kFlightNumber;
    } else if ([elementName isEqualToString:kDepartureAirportCode]) {
        _currentProperty = kDepartureAirportCode;
    } else if ([elementName isEqualToString:kArrivalAirportCode]) {
        _currentProperty = kArrivalAirportCode;
    } else if ([elementName isEqualToString:kDepartureUTC]) {
        _currentProperty = kDepartureUTC;
    } else if ([elementName isEqualToString:kArrivalUTC]) {
        _currentProperty = kArrivalUTC;
    } else if ([elementName isEqualToString:kDepartureTime]) {
        _currentProperty = kDepartureTime;
    } else if ([elementName isEqualToString:kArrivalTime]) {
        _currentProperty = kArrivalTime;
      } else if ([elementName isEqualToString:kDuration]) {
          _currentProperty = kDuration;
      }
}

- (void)parser:(NSXMLParser*)parser foundCharacters:(nonnull NSString *)string {
    if ([_currentProperty isEqualToString:kFlightNumber]) {
        _currentStringValue = string;
    } else if ([_currentProperty isEqualToString:kDepartureAirportCode]) {
        _currentStringValue = string;
    } else if ([_currentProperty isEqualToString:kArrivalAirportCode]) {
        _currentStringValue = string;
    } else if ([_currentProperty isEqualToString:kDepartureUTC]) {
        _currentStringValue = string;
    } else if ([_currentProperty isEqualToString:kArrivalUTC]) {
        _currentStringValue = string;
    } else if ([_currentProperty isEqualToString:kDepartureTime]) {
        _currentStringValue = string;
    } else if ([_currentProperty isEqualToString:kArrivalTime]) {
        _currentStringValue = string;
    } else if ([_currentProperty isEqualToString:kDuration]) {
      _currentStringValue = string;
    }
}

- (void)parser:(NSXMLParser*)parser
 didEndElement:(nonnull NSString *)elementName
  namespaceURI:(nullable NSString *)namespaceURI
 qualifiedName:(nullable NSString *)qName {
    if ([elementName isEqualToString:kAirSegment]) {
        // Как только найден конечный тег текущего элемента
        // Добавляем распарсенную сущность родителю
        // И передаём ему управление NSXMLParser
        [_parent.flight.segments addObject:_segment];
        _parser.delegate = _parent;
    } else if ([elementName isEqualToString:kFlightNumber]) {
        _segment.flightNumber = _currentStringValue;
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if([elementName isEqualToString:kDepartureAirportCode]) {
        _segment.departureAirportCode = _currentStringValue;
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if([elementName isEqualToString:kArrivalAirportCode]) {
        _segment.arrivalAirportCode = _currentStringValue;
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if([elementName isEqualToString:kDepartureUTC]) {
        _segment.departureUTC = [_formatter dateFromString:_currentStringValue];
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if([elementName isEqualToString:kArrivalUTC]) {
        _segment.arrivalUTC = [_formatter dateFromString:_currentStringValue];
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if([elementName isEqualToString:kDepartureTime]) {
        _segment.departureTime = _currentStringValue;
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if([elementName isEqualToString:kArrivalTime]) {
        _segment.arrivalTime = _currentStringValue;
        _currentProperty = nil;
        _currentStringValue = nil;
      } else if([elementName isEqualToString:kDuration]) {
          _segment.duration = [_currentStringValue integerValue];
          _currentProperty = nil;
          _currentStringValue = nil;
      }
}

@end
