// iOS
#import <Foundation/Foundation.h>

// Local
#import "Flight.h"

@class OffersXMLParserDelegate;
@class Offer;

@interface OfferXMLParserDelegate : NSObject<NSXMLParserDelegate>

@property (weak, nonatomic) OffersXMLParserDelegate* parent;
@property (weak, nonatomic) NSXMLParser* parser;
@property (strong, nonatomic) Offer* offer;
@property (assign, nonatomic) Direction currentDirection;

@end
