// Local
#import "OffersXMLParserDelegate.h"
#import "OfferXMLParserDelegate.h"
#import "Offer.h"
#import "Keys.h"

@interface OffersXMLParserDelegate()
@property (strong, nonatomic) NSXMLParser* parser;
@property (strong, nonatomic) NSString* currentProperty;
@property (strong, nonatomic) NSString* currentStringValue;
@property (strong, nonatomic) OfferXMLParserDelegate* child;
@end

@implementation OffersXMLParserDelegate

#pragma mark - NSXMLParserDelegate

- (id)init {
    self = [super init];
    if (self) {
        _offers = [NSMutableArray new];
    }
    return self;
}

- (void)parser:(NSXMLParser*)parser
didStartElement:(nonnull NSString *)elementName
  namespaceURI:(nullable NSString *)namespaceURI
 qualifiedName:(nullable NSString *)qName
    attributes:(nonnull NSDictionary<NSString *,NSString *> *)attributeDict {
      if ([elementName isEqualToString:kOptimalFaresOffer]) {
          _child = nil;
          _child = [OfferXMLParserDelegate new];
          _child.parent = self;
          _child.parser = _parser;
          _parser.delegate = _child;
      }
}

#pragma mark - public methods

- (NSArray*)parseOffers:(NSData *)data error:(NSError *__autoreleasing *)error {
    _parser = [[NSXMLParser alloc] initWithData:data];
    _parser.delegate = self;
    BOOL ok = [_parser parse];
    if (!ok && error) {
        *error = [NSError errorWithDomain:NSXMLParserErrorDomain code:-1 userInfo:nil];
    }
    return _offers;
}

@end
