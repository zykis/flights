// iOS
#import <Foundation/Foundation.h>

@class FlightXMLParserDelegate;

@interface SegmentXMLParserDelegate : NSObject<NSXMLParserDelegate>

@property (weak, nonatomic) NSXMLParser* parser;
@property (weak, nonatomic) FlightXMLParserDelegate* parent;

@end
