// Local
#import "OffersXMLParserDelegate.h"
#import "OfferXMLParserDelegate.h"
#import "FlightXMLParserDelegate.h"
#import "Offer.h"
#import "Keys.h"

@interface OfferXMLParserDelegate()
@property (strong, nonatomic) NSString* currentProperty;
@property (strong, nonatomic) NSString* currentStringValue;
@property(strong, nonatomic) FlightXMLParserDelegate* child;
@end

@implementation OfferXMLParserDelegate

#pragma mark - NSXMLParserDelegate

- (id)init {
    self = [super init];
    if (self) {
        _offer = [Offer new];
    }
    return self;
}

- (void)parser:(NSXMLParser*)parser
didStartElement:(nonnull NSString *)elementName
  namespaceURI:(nullable NSString *)namespaceURI
 qualifiedName:(nullable NSString *)qName
    attributes:(nonnull NSDictionary<NSString *,NSString *> *)attributeDict {
    if ([elementName isEqualToString:kDirection]) {
        _currentProperty = kDirection;
    } else if ([elementName isEqualToString:kTotalPrice]) {
        _currentProperty = kTotalPrice;
    } else if ([elementName isEqualToString:kLink]) {
        _currentProperty = kLink;
    } else if ([elementName isEqualToString:kOptimalFaresFlight]) {
        _child = nil;
        _child = [FlightXMLParserDelegate new];
        _child.direction = _currentDirection;
        _child.parent = self;
        _child.parser = _parser;
        _parser.delegate = _child;
    }
}

- (void)parser:(NSXMLParser*)parser foundCharacters:(nonnull NSString *)string {
    if ([_currentProperty isEqualToString:kDirection]) {
        _currentStringValue = string;
    } else if ([_currentProperty isEqualToString:kTotalPrice]) {
        _currentStringValue = string;
    } else if ([_currentProperty isEqualToString:kLink]) {
        if (_currentStringValue == nil)
            _currentStringValue = @"";
        _currentStringValue = [_currentStringValue stringByAppendingString:string];
    }
}

- (void)parser:(NSXMLParser*)parser
 didEndElement:(nonnull NSString *)elementName
  namespaceURI:(nullable NSString *)namespaceURI
 qualifiedName:(nullable NSString *)qName {
    if ([elementName isEqualToString:kLink]) {
        _offer.link = _currentStringValue;
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if ([elementName isEqualToString:kTotalPrice]) {
        _offer.totalPrice = [_currentStringValue doubleValue];
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if ([elementName isEqualToString:kDirection]) {
        if ([_currentStringValue isEqualToString:@"TO"])
            _currentDirection = TO;
        else if ([_currentStringValue isEqualToString:@"BACK"])
            _currentDirection = BACK;
        else
            @throw [NSException exceptionWithName:@"parser exception" reason:@"no direction found in current string value" userInfo:nil];
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if ([elementName isEqualToString:kOptimalFaresOffer]) {
        [_parent.offers addObject:_offer];
        _parser.delegate = _parent;
    }
}

@end
