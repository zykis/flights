// Local
#import "Keys.h"
#import "FlightXMLParserDelegate.h"
#import "Flight.h"
#import "OfferXMLParserDelegate.h"
#import "Offer.h"
#import "SegmentXMLParserDelegate.h"

@interface FlightXMLParserDelegate()
@property (strong, nonatomic) Flight* segment;
@property (strong, nonatomic) NSString* currentProperty;
@property (strong, nonatomic) NSString* currentStringValue;
@property (strong, nonatomic) SegmentXMLParserDelegate* child;
@end

@implementation FlightXMLParserDelegate

- (id)init {
    self = [super init];
    if (self) {
        self.flight = [Flight new];
    }
    return self;
}

- (void)parser:(NSXMLParser*)parser
didStartElement:(nonnull NSString *)elementName
  namespaceURI:(nullable NSString *)namespaceURI
 qualifiedName:(nullable NSString *)qName
    attributes:(nonnull NSDictionary<NSString *,NSString *> *)attributeDict {
    if ([elementName isEqualToString:kAirSegment]) {
        _child = nil;
        _child = [SegmentXMLParserDelegate new];
        _child.parent = self;
        _child.parser = _parser;
        _parser.delegate = _child;
    } else if ([elementName isEqualToString:kLink]) {
        _currentProperty = kLink;
    }
}

- (void)parser:(NSXMLParser*)parser foundCharacters:(nonnull NSString *)string {
    if ([_currentProperty isEqualToString:kLink]) {
        if (_currentStringValue == nil)
            _currentStringValue = @"";
        _currentStringValue = [_currentStringValue stringByAppendingString:string];
    }
}

- (void)parser:(NSXMLParser*)parser
 didEndElement:(nonnull NSString *)elementName
  namespaceURI:(nullable NSString *)namespaceURI
 qualifiedName:(nullable NSString *)qName {
    if ([elementName isEqualToString:kLink]) {
        _flight.link = _currentStringValue;
        _currentProperty = nil;
        _currentStringValue = nil;
    } else if ([elementName isEqualToString:kOptimalFaresFlight]) {
        if (_direction == TO)
            [_parent.offer.flightsTo addObject:_flight];
        else if (_direction == BACK)
            [_parent.offer.flightsBack addObject:_flight];
        else
            @throw [NSException exceptionWithName:@"parser exception"
                                           reason:@"no direction provided for flight"
                                         userInfo:nil];
        _parser.delegate = _parent;
    }
}

@end
