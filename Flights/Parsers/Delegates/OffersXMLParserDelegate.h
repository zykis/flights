// iOS
#import <Foundation/Foundation.h>

@interface OffersXMLParserDelegate : NSObject<NSXMLParserDelegate>

@property (strong, nonatomic) NSMutableArray* offers;

- (NSArray*)parseOffers:(NSData*)data error:(NSError**)error;

@end
