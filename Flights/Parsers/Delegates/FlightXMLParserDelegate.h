// iOS
#import <Foundation/Foundation.h>

// Local
#import "Flight.h"

@class Flight;
@class OfferXMLParserDelegate;

@interface FlightXMLParserDelegate : NSObject<NSXMLParserDelegate>

@property (weak, nonatomic) NSXMLParser* parser;
@property (weak, nonatomic) OfferXMLParserDelegate* parent;
@property (strong, nonatomic) Flight* flight;
@property (assign, nonatomic) Direction direction;

@end
