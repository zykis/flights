// iOS
#import <Foundation/Foundation.h>

static NSString* const kSessionTokenProperty = @"ns1:session_token";

@interface StartSessionXMLParser : NSObject<NSXMLParserDelegate>

@property (strong, nonatomic) NSString* sessionToken;

- (NSString*)parseStartSession:(NSData*)data error:(NSError**)error;

@end
