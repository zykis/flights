#ifndef Keys_h
#define Keys_h

@class NSString;

static NSString* const kOptimalFaresOffer = @"ns1:GetOptimalFaresOffer";
static NSString* const kOptimalFaresFlight = @"ns1:GetOptimalFaresFlight";
static NSString* const kTotalPrice = @"ns1:total_price";
static NSString* const kDirection = @"ns1:direction";
static NSString* const kAirSegment = @"ns1:AirSegment";
static NSString* const kFlightNumber = @"ns1:flight_number";
static NSString* const kDepartureAirportCode = @"ns1:departure_airport_code";
static NSString* const kArrivalAirportCode = @"ns1:arrival_airport_code";
static NSString* const kDepartureUTC = @"ns1:departure_utc";
static NSString* const kArrivalUTC = @"ns1:arrival_utc";
static NSString* const kLink = @"ns1:link";
static NSString* const kDepartureTime = @"ns1:departure_time";
static NSString* const kArrivalTime = @"ns1:arrival_time";
static NSString* const kDuration = @"ns1:duration";

#endif /* Keys_h */
