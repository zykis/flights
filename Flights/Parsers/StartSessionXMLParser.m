// Local
#import "StartSessionXMLParser.h"

// Libraries
#import <ReactiveObjC/ReactiveObjC.h>

@interface StartSessionXMLParser()
@property (strong, nonatomic) NSXMLParser* parser;
@property (strong, nonatomic) NSString* currentProperty;
@property (strong, nonatomic) NSString* currentStringValue;
@end

@implementation StartSessionXMLParser

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser*)parser didStartElement:(nonnull NSString *)elementName
                                       namespaceURI:(nullable NSString *)namespaceURI
                                      qualifiedName:(nullable NSString *)qName
                                         attributes:(nonnull NSDictionary<NSString *,NSString *> *)attributeDict {
    if ([elementName isEqualToString:kSessionTokenProperty]) {
        // Сбрасываем текстовое значение при смене элемента
        _currentStringValue = nil;
        _currentProperty = kSessionTokenProperty;
    }
}

- (void)parser:(NSXMLParser*)parser foundCharacters:(nonnull NSString *)string {
    if (!_currentStringValue) {
        _currentStringValue = [[NSMutableString alloc] init];
    }
    _currentStringValue = [_currentStringValue stringByAppendingString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(nonnull NSString *)elementName
                                      namespaceURI:(nullable NSString *)namespaceURI
                                     qualifiedName:(nullable NSString *)qName {
    if ([elementName isEqualToString:kSessionTokenProperty]) {
        _sessionToken = _currentStringValue;
    }
}

#pragma mark - public methods

- (NSString*)parseStartSession:(NSData *)data error:(NSError**)error {
    _parser = [[NSXMLParser alloc] initWithData:data];
    [_parser setDelegate:self];
    BOOL ok = [_parser parse];
    if (!ok && error) {
        *error = [NSError errorWithDomain:NSXMLParserErrorDomain code:-1 userInfo:nil];
    }
    return _sessionToken;
}

@end
