// iOS
#import <UIKit/UIKit.h>

@class OffersViewModel;
@class SearchViewModel;

@interface OffersViewController: UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (strong, nonatomic) OffersViewModel* viewModel;
@property (strong, nonatomic) SearchViewModel* searchViewModel;
@property (weak, nonatomic) IBOutlet UIView *loadingView;

@property(weak, nonatomic) IBOutlet UIView* airplane;
@property(weak, nonatomic) IBOutlet UIView* cloudBig;
@property(weak, nonatomic) IBOutlet UIView* cloudMedium;
@property(weak, nonatomic) IBOutlet UIView* cloudSmall;
- (IBAction)animate;

@end
