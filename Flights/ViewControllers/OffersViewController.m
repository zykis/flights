// Local
#import "OffersViewController.h"
#import "OffersViewModel.h"
#import "SearchViewModel.h"
#import "DistinctOfferTableViewCell.h"

// Libraries
#import <ReactiveObjC/ReactiveObjC.h>

static NSString* const kCellIdentifier = @"offer";

@implementation OffersViewController

#pragma mark - Lifecycle

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _viewModel = [OffersViewModel new];
        _searchViewModel = [SearchViewModel new];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    // Устанавливаем даты вылета и возвращения в навигационной панели
    NSDateFormatter* formatter = [NSDateFormatter new];
    formatter.dateFormat = @"dd MMM";
    
    NSString* departureString = [formatter stringFromDate:_searchViewModel.departureDate];
    NSString* returnString = [formatter stringFromDate:_searchViewModel.returnDate];
    NSString* title = [NSString stringWithFormat:@"%@ - %@", departureString, returnString];
    [self.navigationController.navigationBar.topItem setTitle:title];
    
    // Скрываем элементы анимации до её начала
    _cloudBig.hidden = YES;
    _cloudMedium.hidden = YES;
    _cloudSmall.hidden = YES;
    
    // Запускаем анимацию во время поиска
    [self animate];
    
    // Запрашиваем у viewModel данные
    RACSignal* signal = [self.viewModel findOffersWithDepartureAirportCode:_searchViewModel.departureAirportCode
                                                        arrivalAirportCode:_searchViewModel.arrivalAirportCode
                                                             departureDate:_searchViewModel.departureDate
                                                                returnDate:_searchViewModel.returnDate];
    [signal subscribeNext:^(id _Nullable x) {
        NSInteger count = [x integerValue];
        if (count == 0)
            [self presentNoFlightsView];
    } error:^(NSError * _Nullable error) {
        NSLog(@"%@", [error localizedDescription]);
        [self stopAnimate];
    } completed:^{
        [_tableView reloadData];
        [self stopAnimate];
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - UITableViewDataSource delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_viewModel numberOfRowsInSection];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DistinctOfferTableViewCell* cell = [_tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil)
        cell = [[DistinctOfferTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                 reuseIdentifier:kCellIdentifier];

    // Все данные для отображения получаем из viewModel
    // Не давая доступ Контроллеру (View) к Модели. MVVM
    cell.departureTimeToLabel.text = [_viewModel departureTimeToForOfferAtIndex:[indexPath row]];
    cell.arrivalTimeToLabel.text = [_viewModel arrivalTimeToForOfferAtIndex:[indexPath row]];
    cell.departureAirportCodeToLabel.text = [_viewModel departureAirportCodeToForOfferAtIndex:[indexPath row]];
    cell.arrivalAirportCodeToLabel.text = [_viewModel arrivalAirportCodeToForOfferAtIndex:[indexPath row]];

    cell.departureTimeBackLabel.text = [_viewModel departureTimeBackForOfferAtIndex:[indexPath row]];
    cell.arrivalTimeBackLabel.text = [_viewModel arrivalTimeBackForOfferAtIndex:[indexPath row]];
    cell.departureAirportCodeBackLabel.text = [_viewModel departureAirportCodeBackForOfferAtIndex:[indexPath row]];
    cell.arrivalAirportCodeBackLabel.text = [_viewModel arrivalAirportCodeBackForOfferAtIndex:[indexPath row]];

    cell.priceLabel.text = [_viewModel priceForOfferAtIndex:[indexPath row]];
    cell.durationToLabel.text = [_viewModel durationToStringForOfferAtIndex:indexPath.row];
    cell.durationBackLabel.text = [_viewModel durationBackStringForOfferAtIndex:indexPath.row];

    cell.url = [_viewModel urlForOfferAtIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 164.0f;
}

#pragma mark - Animations

- (void)animate {
    // Возвращаем opacity и hidden для loadingView, если было скрыто
    _loadingView.alpha = 1.0;
    _loadingView.hidden = NO;

    CGRect frame = self.view.frame;

    // Добавляем анимацию облака
    CABasicAnimation* cloudBigAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    [cloudBigAnimation setFromValue:[NSValue valueWithCGPoint:CGPointMake(frame.size.width + _cloudBig.frame.size.width / 2, _cloudBig.frame.origin.y + _cloudBig.frame.size.height / 2)]];
    [cloudBigAnimation setToValue:[NSValue valueWithCGPoint:CGPointMake(-_cloudBig.frame.size.width / 2, _cloudBig.frame.origin.y + _cloudBig.frame.size.height / 2)]];
    [cloudBigAnimation setDuration:1.5];
    [cloudBigAnimation setBeginTime:0.0];
    [cloudBigAnimation setRepeatCount:HUGE_VALF];
    [_cloudBig.layer addAnimation:cloudBigAnimation forKey:nil];
    _cloudBig.hidden = NO;
    
    // Добавляем анимацию облака
    CABasicAnimation* cloudMedAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    [cloudMedAnimation setFromValue:[NSValue valueWithCGPoint:CGPointMake(frame.size.width + _cloudMedium.frame.size.width / 2, _cloudMedium.frame.origin.y + _cloudMedium.frame.size.height / 2)]];
    [cloudMedAnimation setToValue:[NSValue valueWithCGPoint:CGPointMake(-_cloudMedium.frame.size.width / 2, _cloudMedium.frame.origin.y + _cloudMedium.frame.size.height / 2)]];
    [cloudMedAnimation setDuration:4.5];
    [cloudMedAnimation setBeginTime:0.0];
    [cloudMedAnimation setRepeatCount:HUGE_VALF];
    [_cloudMedium.layer addAnimation:cloudMedAnimation forKey:nil];
    _cloudMedium.hidden = NO;
    
    // Добавляем анимацию облака
    CABasicAnimation* cloudSmallAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    [cloudSmallAnimation setFromValue:[NSValue valueWithCGPoint:CGPointMake(frame.size.width + _cloudSmall.frame.size.width / 2, _cloudSmall.frame.origin.y + _cloudSmall.frame.size.height / 2)]];
    [cloudSmallAnimation setToValue:[NSValue valueWithCGPoint:CGPointMake(-_cloudSmall.frame.size.width / 2, _cloudSmall.frame.origin.y + _cloudSmall.frame.size.height / 2)]];
    [cloudSmallAnimation setDuration:12.5];
    [cloudSmallAnimation setBeginTime:0.0];
    [cloudSmallAnimation setRepeatCount:HUGE_VALF];
    [_cloudSmall.layer addAnimation:cloudSmallAnimation forKey:nil];
    _cloudSmall.hidden = NO;
}

- (void)stopAnimate {
    _cloudSmall.hidden = YES;
    _cloudMedium.hidden = YES;
    _cloudBig.hidden = YES;
    [_cloudBig.layer removeAllAnimations];
    [_cloudMedium.layer removeAllAnimations];
    [_cloudSmall.layer removeAllAnimations];
    [UIView animateWithDuration:0.3
                     animations:^{
                         _loadingView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         _loadingView.hidden = YES;
                     }];
}

- (void)presentNoFlightsView {
    UIView* noFlightsView = [[UIView alloc] initWithFrame:self.view.frame];
    noFlightsView.backgroundColor = [UIColor whiteColor];

    CGRect imageViewFrame = self.view.frame;
    imageViewFrame.origin.y = self.view.frame.size.height - 250;
    imageViewFrame.origin.x = (self.view.frame.size.width - 200) / 2;
    imageViewFrame.size.height = 200;
    imageViewFrame.size.width = 200;
    UIImageView* noFlightsImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sad-200.png"]];
    noFlightsImageView.frame = imageViewFrame;
    noFlightsImageView.contentMode = UIViewContentModeScaleAspectFill;

    CGRect labelFrame = self.view.frame;
    labelFrame.origin.y = 100;
    labelFrame.size.height = 200;
    UILabel* noFlightsLabel = [[UILabel alloc] initWithFrame:labelFrame];
    noFlightsLabel.numberOfLines = 2;
    noFlightsLabel.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1.0];
    noFlightsLabel.text = @"Нет авиаперелётов на указанные даты";
    noFlightsLabel.textColor = [UIColor colorWithWhite:0.0 alpha:0.57];
    noFlightsLabel.textAlignment = NSTextAlignmentCenter;
    UIFont* font = [UIFont systemFontOfSize:24.0 weight:UIFontWeightBold];
    noFlightsLabel.font = font;

    [noFlightsView addSubview:noFlightsImageView];
    [noFlightsView addSubview:noFlightsLabel];
    [self.view addSubview:noFlightsView];
}

@end
