// iOS
#import <UIKit/UIKit.h>

@class SearchViewModel;
@class FSCalendar;
@protocol FSCalendarDataSource;
@protocol FSCalendarDelegate;

typedef enum { CT_NONE, CT_TO, CT_RETURN } CalendarType;

@interface SearchViewController: UIViewController<UIGestureRecognizerDelegate>

@property (strong, nonatomic) FSCalendar* calendar;
@property (weak, nonatomic) IBOutlet UIStackView *toDate;
@property (weak, nonatomic) IBOutlet UIStackView *returnDate;
@property (weak, nonatomic) IBOutlet UILabel *departureAirportCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *arrivalAirportCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *departureDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *returnDateLabel;

@property (assign, nonatomic) BOOL calendarHidden;
@property (assign, nonatomic) CalendarType calendarType;
@property (strong, nonatomic) NSDateComponents* departureDateComponents;
@property (strong, nonatomic) NSDateComponents* returnDateComponents;

@property (strong, nonatomic) SearchViewModel* viewModel;

@property (weak, nonatomic) IBOutlet UIButton* findButton;
- (IBAction)findPressed;
@end
