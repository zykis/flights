// Local
#import "SearchViewController.h"
#import "OffersViewController.h"
#import "SearchViewModel.h"

// Libararies
#import <FSCalendar/FSCalendar.h>
#import <ReactiveObjC/ReactiveObjC.h>

@interface SearchViewController()<FSCalendarDataSource, FSCalendarDelegate>
@property (strong, nonatomic) UITapGestureRecognizer* mainTapGestureRecognizer;
@property (strong, nonatomic) UITapGestureRecognizer* toGestureRecognizer;
@property (strong, nonatomic) UITapGestureRecognizer* returnGestureRecognizer;
@end

@implementation SearchViewController

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _viewModel = [SearchViewModel new];
    _departureDateComponents = [NSDateComponents new];
    _returnDateComponents = [NSDateComponents new];

    // Добавляем UITapGestureRecognizer для основного UIView, чтобы скрывать календарь
    _mainTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideCalendar)];
    _mainTapGestureRecognizer.delegate = self; // для того, чтобы не перекрывать последующие UITapGestureRecognizer
    [self.view addGestureRecognizer:_mainTapGestureRecognizer];

    // Добавляем UITapGestureRecognizer для даты вылета
    _toGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toDateTapped)];
    [_toDate addGestureRecognizer:_toGestureRecognizer];

    // Добавляем UITapGestureRecognizer для даты возвращения
    _returnGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(returnDateTapped)];
    [_returnDate addGestureRecognizer:_returnGestureRecognizer];

    // Добавляем Calendar под главный View
    _calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0,
                                                             self.view.frame.size.height,
                                                             self.view.frame.size.width,
                                                             300)];
    _calendar.dataSource = self;
    _calendar.delegate = self;
    [self.view addSubview:_calendar];

    // Помечаем, что календарь скрыт
    _calendarHidden = YES;

    // Настраиваем вид календаря
    _calendar.backgroundColor = [UIColor whiteColor];
    _calendar.today = nil;

    // Смотрим за выбранными датами и меняем Label'ы даты соответственно
    NSDateFormatter* formatter = [NSDateFormatter new];
    formatter.dateFormat = @"dd MMM";

    [RACObserve(_viewModel, departureDate) subscribeNext:^(id  _Nullable x) {
        _departureDateLabel.text = [formatter stringFromDate:[_viewModel departureDate]];
    }];
    [RACObserve(_viewModel, returnDate) subscribeNext:^(id  _Nullable x) {
        _returnDateLabel.text = [formatter stringFromDate:[_viewModel returnDate]];
    }];

    // Кнопка поиска активна только при валидных датах
    RACSignal* s = [RACSignal combineLatest:@[ [RACObserve(_viewModel, departureDate) ignore:nil],
                                                [RACObserve(_viewModel, returnDate) ignore:nil] ]
                                      reduce:^id _Nullable (NSDate* departureDate, NSDate* returnDate){
                                          if (departureDate == nil || returnDate == nil) return false;
                                          NSCalendar* gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                                          NSDateComponents* componentsDeparture = [gregorian components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:departureDate];
                                          NSDateComponents* componentsReturn = [gregorian components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:returnDate];
                                          NSDateComponents* componentsToday = [gregorian components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:[NSDate new]];
                                          BOOL isReturnAfterDeparture = componentsReturn.year > componentsDeparture.year ? YES : componentsReturn.year < componentsDeparture.year ? NO :
                                          componentsReturn.month > componentsDeparture.month ? YES : componentsReturn.month < componentsDeparture.month ? NO :
                                          componentsReturn.day > componentsDeparture.day ? YES : componentsReturn.day < componentsDeparture.day ? NO : YES;
                                          BOOL isDepartureAfterToday = componentsDeparture.year > componentsToday.year ? YES : componentsDeparture.year < componentsToday.year ? NO :
                                          componentsDeparture.month > componentsToday.month ? YES : componentsDeparture.month < componentsToday.month ? NO :
                                          componentsDeparture.day > componentsToday.day ? YES : componentsDeparture.day < componentsToday.day ? NO : YES;
                                          BOOL ok = isReturnAfterDeparture && isDepartureAfterToday;
                                          return @(ok);
                                      }];
    [_findButton setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.54] forState:UIControlStateDisabled];
    RAC(self, findButton.enabled) = s;
}

#pragma mark - FSCalendarDelegate

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    if (_calendarType == CT_TO) {
        [_viewModel setDepartureDate:date];
    } else if (_calendarType == CT_RETURN) {
        [_viewModel setReturnDate:date];
    }
    if (!_calendarHidden) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect destRect = CGRectMake(0,
                                         self.view.frame.size.height,
                                         _calendar.frame.size.width,
                                         _calendar.frame.size.height);
            _calendar.frame = destRect;
        } completion:^(BOOL finished) {
            _calendarHidden = YES;
        }];
    }
}

- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date
{
    NSDate *now = [NSDate date];
    NSCalendar *c = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components = [c components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    NSDate *today = [c dateFromComponents:components];
    float ti = [date timeIntervalSinceDate:today];
    if (ti < 0) {
        return NO;
    }
    return YES;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![touch.view isDescendantOfView:_calendar];;
}

#pragma mark - Actions

- (IBAction)findPressed {
    if (_viewModel.departureDate == nil || _viewModel.returnDate == nil) return;

    [self.viewModel setDepartureAirportCode:_departureAirportCodeLabel.text];
    [self.viewModel setArrivalAirportCode:_arrivalAirportCodeLabel.text];

    [self performSegueWithIdentifier:@"showFlights" sender:self];
}

- (void)toDateTapped {
    _calendarType = CT_TO;
    if (!_calendarHidden) return;
    [self showCalendar];
}

- (void)returnDateTapped {
    _calendarType = CT_RETURN;
    if (!_calendarHidden) return;
    [self showCalendar];
}

- (void)hideCalendar {
    if (_calendarHidden) return;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect destRect = CGRectMake(0,
                                     self.view.frame.size.height,
                                     _calendar.frame.size.width,
                                     _calendar.frame.size.height);
        _calendar.frame = destRect;
    } completion:^(BOOL finished) {
        _calendarHidden = YES;
    }];
}

- (void)showCalendar {
    if (!_calendarHidden) return;
    // Выбираем текущую дату календаря в соответствии с датой, которую мы хотим оторбазить
    if ((_calendarType == CT_TO) && ([_viewModel departureDate] != nil))
        [_calendar selectDate:[_viewModel departureDate]];
    else if ((_calendarType == CT_RETURN) && ([_viewModel returnDate] != nil))
        [_calendar selectDate:[_viewModel returnDate]];
    [UIView animateWithDuration:0.3 animations:^{
        CGRect destRect = CGRectMake(0,
                                     self.view.frame.size.height - 300,
                                     _calendar.frame.size.width,
                                     _calendar.frame.size.height);
        _calendar.frame = destRect;
    } completion:^(BOOL finished) {
        _calendarHidden = NO;
    }];
}

#pragma mark Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showFlights"]) {
        OffersViewController* destVC = (OffersViewController*)[segue destinationViewController];
        //! TODO: copy fields instead of assign?
        destVC.searchViewModel = self.viewModel;
    }
}
@end
