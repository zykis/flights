@import UIKit;
#import "Flight.h"

@class DistinctOffer;

@interface DistinctOfferTableViewCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel* departureTimeToLabel;
@property (weak, nonatomic) IBOutlet UILabel* arrivalTimeToLabel;
@property (weak, nonatomic) IBOutlet UILabel* departureAirportCodeToLabel;
@property (weak, nonatomic) IBOutlet UILabel* arrivalAirportCodeToLabel;
@property (weak, nonatomic) IBOutlet UILabel* durationToLabel;

@property (weak, nonatomic) IBOutlet UILabel* departureTimeBackLabel;
@property (weak, nonatomic) IBOutlet UILabel* arrivalTimeBackLabel;
@property (weak, nonatomic) IBOutlet UILabel* departureAirportCodeBackLabel;
@property (weak, nonatomic) IBOutlet UILabel* arrivalAirportCodeBackLabel;
@property (weak, nonatomic) IBOutlet UILabel* durationBackLabel;

@property (weak, nonatomic) IBOutlet UILabel* priceLabel;

@property (strong, nonatomic) NSURL* url;
- (IBAction)buyPressed;

@end
