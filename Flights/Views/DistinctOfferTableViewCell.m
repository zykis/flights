// Local
#import "DistinctOfferTableViewCell.h"

@implementation DistinctOfferTableViewCell

- (IBAction)buyPressed {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
      [[UIApplication sharedApplication] openURL:_url options:@{} completionHandler:nil];
    }
}

@end
