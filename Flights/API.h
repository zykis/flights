#import <ReactiveObjC/ReactiveObjC.h>

@interface API: NSObject

+ (API*)shared;

- (RACSignal*)loginWithUsername:(NSString*)username
                       password:(NSString*)password
                           hash:(NSString*)hash
                    disableHash:(BOOL)disableHash;

- (RACSignal*)findOffersWithToken:(NSString*)token
                    departureDate:(NSDate*)departureDate
                       returnDate:(NSDate*)returnDate
             departureAirportCode:(NSString*)departureAirportCode
               arrivalAirportCode:(NSString*)arrivalAirportCode;

@end
