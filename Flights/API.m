// Local
#import "API.h"
#import "StartSessionXMLParser.h"
#import "OffersXMLParserDelegate.h"

// Libraries
#import <AFNetworking/AFNetworking.h>

@implementation API

static NSString* const ENDPOINT = @"https://search.biletix.ru/bitrix/components/travelshop/ibe.soap/travelshop_booking.php";

#pragma mark - Singleton

+ (API*)shared {
    static API *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

#pragma mark - API calls

- (RACSignal*)loginWithUsername:(NSString*)username
                       password:(NSString*)password
                           hash:(NSString*)hash
                    disableHash:(BOOL)disableHash {
    RACReplaySubject* subject = [RACReplaySubject subject];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml", nil];
    manager.responseSerializer = responseSerializer;

    NSURL *URL = [NSURL URLWithString:ENDPOINT];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    NSString* bodyString = [NSString stringWithFormat:@"<SOAP-ENV:Envelope "
                                                    "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                                                    "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" "
                                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                                                    "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
                                                        "<SOAP-ENV:Body>"
                                                            "<m:StartSessionInput xmlns:m=\"http://www.tais.ru/\">"
                                                            "<m:login>%@</m:login>"
                                                            "<m:password>%@</m:password>"
                                                            "<m:hash>%@</m:hash>"
                                                            "<m:disable_hash>%@</m:disable_hash>"
                                                        "</m:StartSessionInput>"
                                                    "</SOAP-ENV:Body>"
                                                    "</SOAP-ENV:Envelope>", username, password, hash, disableHash ? @"Y" : @"N"];

    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];

    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            [subject sendError:error];
        } else {
            NSData * data = (NSData *)responseObject;
            StartSessionXMLParser* parser = [StartSessionXMLParser new];
            NSError* error;

            NSString* token = [parser parseStartSession:data error:&error];
            if (error) {
                [subject sendError:error];
            } else {
                [subject sendNext:token];
                [subject sendCompleted];
            }
        }
    }];
    [dataTask resume];
    return subject;
}

- (RACSignal*)findOffersWithToken:(NSString *)token
                    departureDate:(NSDate *)departureDate
                       returnDate:(NSDate *)returnDate
             departureAirportCode:(NSString *)departureAirportCode
               arrivalAirportCode:(NSString *)arrivalAirportCode {
    RACReplaySubject* subject = [RACReplaySubject subject];

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml", nil];
    manager.responseSerializer = responseSerializer;

    NSURL *URL = [NSURL URLWithString:ENDPOINT];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];

    NSDateFormatter* formatter = [NSDateFormatter new];
    formatter.dateFormat = @"dd.MM.yyyy";
    NSString* bodyString = [NSString stringWithFormat:@"<SOAP-ENV:Envelope "
                            "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                            "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" "
                            "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                            "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
                                "<SOAP-ENV:Body>"
                                    "<m:GetOptimalFaresInput xmlns:m=\"http://www.tais.ru/\">"
                                        "<m:session_token>%@</m:session_token>"
                                        "<m:hash>29a51904334ef3c1a50a0610d6e59960</m:hash>"
                                        "<m:owrt>RT</m:owrt>"
                                        "<m:departure_point>%@</m:departure_point>"
                                        "<m:arrival_point>%@</m:arrival_point>"
                                        "<m:outbound_date>%@</m:outbound_date>"
                                        "<m:outbound_time_range></m:outbound_time_range>"
                                        "<m:return_date>%@</m:return_date>"
                                        "<m:return_time_range></m:return_time_range>"
                                        "<m:adult_count>1</m:adult_count>"
                                        "<m:child_count></m:child_count>"
                                        "<m:infant_count></m:infant_count>"
                                        "<m:class></m:class>"
                                        "<m:direct_only></m:direct_only>"
                                        "<m:promocode></m:promocode>"
                                    "</m:GetOptimalFaresInput>"
                                "</SOAP-ENV:Body>"
                            "</SOAP-ENV:Envelope>",
                            token,
                            departureAirportCode,
                            arrivalAirportCode,
                            [formatter stringFromDate:departureDate],
                            [formatter stringFromDate:returnDate]];

    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];

    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            [subject sendError:error];
        } else {
            NSData * data = (NSData *)responseObject;
            OffersXMLParserDelegate* parser = [OffersXMLParserDelegate new];
            NSError* error;

            NSArray* offers = [parser parseOffers:data error:&error];
            if (error) {
                [subject sendError:error];
            } else {
                [subject sendNext:offers];
                [subject sendCompleted];
            }
        }
    }];
    [dataTask resume];
    return subject;
}

@end
