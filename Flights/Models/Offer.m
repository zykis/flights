#import "Offer.h"
#import "DistinctOffer.h"
#import "Flight.h"
#import "Segment.h"

@implementation Offer

- (id)init {
    self = [super init];
    if (self) {
        _flightsTo = [NSMutableArray array];
        _flightsBack = [NSMutableArray array];
    }
    return self;
}

// Метод разделения предожений с множественными полётами на
// предложения с единственным полётом в каждую сторону
- (NSArray<DistinctOffer*>*)distinctOffers {
    NSMutableArray* rv = [NSMutableArray array];
    for (Flight* flightTo in _flightsTo) {
        for (Flight* flightBack in _flightsBack) {
            DistinctOffer* distinctOffer = [DistinctOffer new];
            distinctOffer.departureDate = [flightTo.segments firstObject].departureUTC;
            distinctOffer.returnDate = [flightBack.segments firstObject].departureUTC;
            distinctOffer.departureAirportCode = [flightTo.segments firstObject].departureAirportCode;
            distinctOffer.arrivalAirportCode = [flightTo.segments lastObject].arrivalAirportCode;
            distinctOffer.totalPrice = self.totalPrice;
            distinctOffer.link = [[self.link stringByAppendingString:flightTo.link]
                                             stringByAppendingString:flightBack.link];
            distinctOffer.flightTo = flightTo;
            distinctOffer.flightBack = flightBack;

            // Считаем время полёта туда
            NSInteger durationTo = 0;
            for (Segment* s in flightTo.segments) {
                durationTo += s.duration;
            }
            distinctOffer.durationTo = durationTo;

            // Считаем время полёта обратно
            NSInteger durationBack = 0;
            for (Segment* s in flightBack.segments) {
                durationBack += s.duration;
            }
            distinctOffer.durationBack = durationBack;

            [rv addObject:distinctOffer];
        }
    }
    return [NSArray arrayWithArray:rv];
}

@end
