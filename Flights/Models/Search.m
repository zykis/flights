// Local
#import "Search.h"

@implementation Search

- (id)init {
    self = [super init];
    if (self) {
        self.departureAirportCode = [NSString new];
        self.arrivalAirportCode = [NSString new];
        self.departureDate = [NSDate new];
        self.returnDate = [NSDate new];
    }
    return self;
}

@end
