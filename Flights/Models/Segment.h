// iOS
#import <Foundation/Foundation.h>

@interface Segment : NSObject

@property (strong, nonatomic) NSString* flightNumber;
@property (strong, nonatomic) NSString* departureAirportCode;
@property (strong, nonatomic) NSString* arrivalAirportCode;
@property (strong, nonatomic) NSDate* departureUTC;
@property (strong, nonatomic) NSDate* arrivalUTC;
@property (strong, nonatomic) NSString* departureTime;
@property (strong, nonatomic) NSString* arrivalTime;
@property (assign, nonatomic) NSInteger duration;

@end
