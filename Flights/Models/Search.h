// iOS
#import <Foundation/Foundation.h>

@interface Search : NSObject

@property (strong, nonatomic) NSString* departureAirportCode;
@property (strong, nonatomic) NSString* arrivalAirportCode;
@property (strong, nonatomic) NSDate* departureDate;
@property (strong, nonatomic) NSDate* returnDate;

@end
