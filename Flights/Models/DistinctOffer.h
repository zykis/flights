// iOS
#import <Foundation/Foundation.h>

@class Flight;

@interface DistinctOffer: NSObject

@property (strong, nonatomic) NSDate* departureDate;
@property (strong, nonatomic) NSDate* returnDate;
@property (strong, nonatomic) NSString* departureAirportCode;
@property (strong, nonatomic) NSString* arrivalAirportCode;
@property (assign, nonatomic) double totalPrice;
@property (strong, nonatomic) Flight* flightTo;
@property (strong, nonatomic) Flight* flightBack;
@property (assign, nonatomic) NSInteger durationTo;
@property (assign, nonatomic) NSInteger durationBack;
@property (strong, nonatomic) NSString* link;

@end
