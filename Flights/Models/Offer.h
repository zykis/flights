// iOS
#import <Foundation/Foundation.h>

@class Flight;
@class DistinctOffer;

@interface Offer: NSObject

@property (assign, nonatomic) double totalPrice;
@property (strong, nonatomic) NSMutableArray<Flight*>* flightsTo;
@property (strong, nonatomic) NSMutableArray<Flight*>* flightsBack;
@property (strong, nonatomic) NSString* link;

- (NSArray<DistinctOffer*>*)distinctOffers;

@end
