// iOS
#import <Foundation/Foundation.h>

@class Segment;

typedef enum { NONE, TO, BACK } Direction;

@interface Flight : NSObject

@property (strong, nonatomic) NSMutableArray<Segment*>* segments;
@property (strong, nonatomic) NSString* link;

@end
